package com.example.demo.controller;


import com.example.demo.model.LoginUser;
import com.example.demo.repository.LoginUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class LoginController {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
    @Autowired
    LoginUserRepository loginUserRepository;

    @RequestMapping(value = "/login",
            method = RequestMethod.POST)
    public LoginUser postUser(@RequestBody LoginUser loginUser){
        loginUserRepository.save(loginUser);
        logger.info("++++++++++++++++++++success----------------------");
        return loginUser;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public LoginUser getUser( @RequestParam("userName") String userName){

        logger.info("get duoc roi");
        LoginUser loginUser = loginUserRepository.getUser(userName);
        return loginUser;
    }

}

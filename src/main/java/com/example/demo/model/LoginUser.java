package com.example.demo.model;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "login")
public class LoginUser {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    @Column(name = "login")
    private String loginUser;
    @Column(name = "password")
    private String passWord;

}

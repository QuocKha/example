package com.example.demo.repository;


import com.example.demo.model.LoginUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginUserRepository extends JpaRepository<LoginUser, Long> {

    @Query(value = "select * from login where login =:userName",nativeQuery = true)
    LoginUser getUser(@Param("userName") String userName);
}
